"""Stream type classes for tap-livechat."""

from __future__ import annotations

from pathlib import Path

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_livechat.client import livechatStream

# TODO: Delete this is if not using json files for schema definition
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.


UserType = th.ObjectType(
    th.Property("id", th.StringType),
    th.Property("name", th.StringType),
    th.Property("email", th.StringType),
    th.Property("type", th.StringType),
    th.Property("present", th.BooleanType),
    th.Property("events_seen_up_to", th.StringType),
    th.Property("created_at", th.StringType),
    th.Property("last_visit", th.ObjectType(
        th.Property("id", th.IntegerType),
        th.Property("started_at", th.StringType),
        th.Property("ended_at", th.StringType),
        th.Property("ip", th.StringType),
        th.Property("user_agent", th.StringType),
        th.Property("geolocation", th.ObjectType(
            th.Property("country", th.StringType),
            th.Property("country_code", th.StringType),
            th.Property("region", th.StringType),
            th.Property("city", th.StringType),
            th.Property("timezone", th.StringType),
            th.Property("latitude", th.StringType),
            th.Property("longitude", th.StringType),
        )),
        th.Property("last_pages", th.ArrayType(
            th.ObjectType(
                th.Property("opened_at", th.StringType),
                th.Property("url", th.StringType),
                th.Property("title", th.StringType),
            )
        ))
    )),
    th.Property("statistics", th.ObjectType(
        th.Property("chats_count", th.IntegerType),
        th.Property("threads_count", th.IntegerType),
        th.Property("visits_count", th.IntegerType),
        th.Property("page_views_count", th.IntegerType),
        th.Property("greetings_shown_count", th.IntegerType),
        th.Property("greetings_accepted_count", th.IntegerType),
    )),
    th.Property("agent_last_event_created_at", th.StringType),
    th.Property("customer_last_event_created_at", th.StringType),
    th.Property("email_verified", th.BooleanType),
    th.Property("avatar", th.StringType),
    th.Property("visibility", th.StringType)
)

class ListChatsStream(livechatStream):
    name = "listChats"
    path = "/agent/action/list_chats"
    records_jsonpath = "$.chats_summary[*]"
    primary_keys = ["id"]
    replication_key = None

    def prepare_request_payload(self, context: dict | None, next_page_token: Any | None) -> dict | None:
        return {
            "page_id": next_page_token,
        }

    def get_next_page_token(self, response: requests.Response, previous_token: Any | None) -> Any | None:
        return response.json().get("next_page_id")

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("last_event_per_type", th.CustomType({"type": ["object"]})),
        th.Property("users", th.ArrayType(UserType)),
        th.Property("last_thread_summary", th.CustomType({"type": ["object"]})),
        th.Property("properties", th.CustomType({"type": ["object"]})),
        th.Property("access", th.ObjectType(
            th.Property("group_ids", th.ArrayType(th.IntegerType))
        )),
        th.Property("is_followed", th.BooleanType)
    ).to_dict()

    def get_child_context(self, record: dict, context: dict | None) -> dict:
        if record:
            return {
                "chat_id": record["id"],
                "thread_id": record["last_thread_summary"]["id"]
            }
        else:
            return None


class ChatStream(livechatStream):
    name = "Chat"
    path = "/agent/action/get_chat"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = ListChatsStream

    def prepare_request_payload(
        self,
        context: dict | None,
        next_page_token: Any | None,
    ) -> dict | None:
        return {
            "chat_id": context["chat_id"],
            "thread_id": context["thread_id"]
        }

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("users", th.ArrayType(UserType)),
        th.Property("thread", th.CustomType({"type": ["object"]})),
        th.Property("properties", th.CustomType({"type": ["object"]})),
        th.Property("access", th.ObjectType(
            th.Property("group_ids", th.ArrayType(th.IntegerType))
        )),
        th.Property("is_followed", th.BooleanType),
    ).to_dict()
    
